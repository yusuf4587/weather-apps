package com.yusuf.app.weather_apps.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.RoomDatabase
import com.yusuf.app.weather_apps.utils.StringUtil

data class WeatherResponse(
    val coord: Coord,
    val weather: List<Weather>,
    val base: String,
    val main: Main,
    val visibility: String,
    val wind: Wind,
    val clouds: Clouds,
    val dt: Int,
    val sys: Sys,
    val timeZone: Int,
    val id: Int,
    val name: String,
    val cod: Int
) {
    data class Coord(
        val lon: Double,
        val lat: Double,
    )

    data class Weather(
        val id: Int,
        val main: String,
        val description: String,
        val icon: String,
    )

    data class Main(
        val temp: Double,
        val feels_like: Double,
        val temp_min: Double,
        val temp_max: Double,
        val pressure: Int,
        val humidity: Int,
        val sea_level: Int,
        val grnd_level: Int,
    )

    data class Wind(
        val sepeed: Double,
        val deg: Int,
        val gust: Double,
    )

    data class Clouds(
        val all: Int
    )

    data class Sys(
        val type: Int,
        val id: Int,
        val country: String,
        val sunrise: Int,
        val sunset: Int
    )
}


data class WeatherForecastResponse(
    val lat:Double,
    val lon: Double,
    val timezone:String,
    val timezone_offset:Int,
    val current:Current,
    val daily:List<Daily>
){
    data class Current(
        val dt:Int,
        val sunrise: Int,
        val sunset: Int,
        val temp : Double,
        val feels_like: Double,
        val pressure: Int,
        val humidity: Int,
        val dew_point:Double,
        val uvi:Double,
        val clouds: Int,
        val visibility: String,
        val wind_speed:Double,
        val wind_deg:Int,
        val wind_gust:Double,
        val weather: List<WeatherResponse.Weather>
    )

    data class Daily(
        val dt: Int,
        val sunrise: Int,
        val sunset: Int,
        val moonrise:Int,
        val moon_phase:Double,
        val temp:Temp,
        val feels_like:FeelsLike,
        val pressure: Int,
        val humidity: Int,
        val dew_point: Double,
        val wind_speed: Double,
        val wind_deg: Int,
        val wind_gust: Double,
        val weather: List<WeatherResponse.Weather>,
        val clouds: Int,
        val pop:Double,
        val rain:Double,
        val uvi:Double
    ){
        data class Temp(
            val day:Double,
            val min:Double,
            val max:Double,
            val night:Double,
            val eve:Double,
            val morn:Double,
        )
        data class FeelsLike(
            val day:Double,
            val night:Double,
            val eve:Double,
            val morn:Double,
        )
    }
}

@Entity(tableName = "WeatherTable")
data class WeatherTable(
    @PrimaryKey var id:Int ,
    val dt :Int,
    val date : String,
    val city : String,
    val cityResponse:String,
    val lat:Double,
    val lon:Double,
    val temp:Double,
    val humidity: Int,
    val pressure: Int,
    val weather_name:String,
    val icon:String,
){
    companion object {
        fun ResponseToTable(
            data:WeatherResponse,
            stringUtil: StringUtil,
            city:String
        )=WeatherTable(
            data.id,
            data.dt,
            stringUtil.convertLongToTime(data.dt.toLong())+" \n" + city+"," +stringUtil.getCountryName(data.sys.country),
            city,
            data.name,
            data.coord.lat,
            data.coord.lon,
            stringUtil.getAverange(data.main.temp_min,data.main.temp_max),
            data.main.humidity,
            data.main.pressure,
            data.weather[0].description,
            data.weather[0].icon,
        )
    }
}

@Entity(tableName = "forecastTable")
data class ForecastTable(
    @PrimaryKey(autoGenerate = true) var id:Int = 0,
    val weather_id:Int,
    val icon:String,
    val dt:Int,
    val day_name:String,
    val temp_average:Double
){
    companion object{
        fun ResponsetoTable(
            data: WeatherForecastResponse.Daily,
            weather_id: Int,
            stringUtil: StringUtil
        )=ForecastTable(
        0,
        weather_id,
        data.weather[0].icon,
        data.dt,
        stringUtil.getDayName(data.dt.toLong()),
            stringUtil.getAverange(data.temp.min,data.temp.max),
        )
    }
}