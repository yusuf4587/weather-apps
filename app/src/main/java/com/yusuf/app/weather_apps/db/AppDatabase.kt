package com.yusuf.app.weather_apps.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.yusuf.app.weather_apps.model.ForecastTable
import com.yusuf.app.weather_apps.model.WeatherTable


@Database(
    entities = [
        WeatherTable::class,
        ForecastTable::class
    ],
    exportSchema = false,
    version = 4
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun weatherDao(): WeatherDao
    abstract fun forecastDao(): ForecastDao
}