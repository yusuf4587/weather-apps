package com.yusuf.app.weather_apps.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.yusuf.app.weather_apps.databinding.ListItemBinding

class FilterAdapter(private val listener: FilterListener,private val bottomSheetDialog: BottomSheetDialog) :
    RecyclerView.Adapter<FilteViewHolder>() {

    interface FilterListener {
        fun onClickedShortBy(data: String)
    }


    private val items = ArrayList<String>()

    fun setItems(items: List<String> = emptyList()) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilteViewHolder {
        val binding: ListItemBinding =
            ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FilteViewHolder(binding,listener,bottomSheetDialog)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: FilteViewHolder, position: Int) =
        holder.bind(items[position], position)
}

class FilteViewHolder(
    private val itemBinding: ListItemBinding,
    private val listener: FilterAdapter.FilterListener,
    private val bottomSheetDialog: BottomSheetDialog
) : RecyclerView.ViewHolder(itemBinding.root) {

    private lateinit var data: String

    @SuppressLint("SetTextI18n")
    fun bind(item: String, position: Int) {
        this.data = item
        itemBinding.name.text = data

        itemBinding.name.setOnClickListener {
            bottomSheetDialog.dismiss()
            listener.onClickedShortBy(data)
        }
    }
}