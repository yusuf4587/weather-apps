package com.yusuf.app.weather_apps.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yusuf.app.weather_apps.model.WeatherTable
import kotlinx.coroutines.flow.Flow

@Dao
interface WeatherDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(weather: WeatherTable)

    @Query("SELECT * FROM weatherTable where city =:city ")
    fun getWeather(city: String): Flow<WeatherTable>

    @Query("SELECT count(*) FROM weatherTable where city =:city   ")
    suspend fun countWeather(city: String): Int
}