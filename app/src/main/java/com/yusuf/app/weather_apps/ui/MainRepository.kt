package com.yusuf.app.weather_apps.ui

import com.yusuf.app.weather_apps.db.ForecastDao
import com.yusuf.app.weather_apps.db.WeatherDao
import com.yusuf.app.weather_apps.model.ForecastTable
import com.yusuf.app.weather_apps.model.WeatherForecastResponse
import com.yusuf.app.weather_apps.model.WeatherResponse
import com.yusuf.app.weather_apps.model.WeatherTable

import com.yusuf.app.weather_apps.network.ApiService
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val api : ApiService,
    private val forecastDao: ForecastDao,
    private val weatherDao: WeatherDao
){

    suspend fun fetchWeather(city:String):Response<WeatherResponse>{
        return api.getWeather(city)
    }

    suspend fun fetchForecast(lat:Double,lon:Double):Response<WeatherForecastResponse>{
        return api.getForecast(lat,lon)
    }

    fun getWeather(city:String ): Flow<WeatherTable> {
        return weatherDao.getWeather(city)
    }

    suspend fun countWeather(city:String ): Int{
        return weatherDao.countWeather(city)
    }

    fun getForecast(weather:Int ): Flow<List<ForecastTable>> {
        return forecastDao.getForecast(weather)
    }

    suspend fun insertWeather(weatherTable: WeatherTable){
        return weatherDao.insert(weatherTable)
    }

    suspend fun insertForecast(forecastTable: ForecastTable){
        return forecastDao.insert(forecastTable)
    }

    suspend fun countForecast(weather: Int ): Int{
        return forecastDao.countForecast(weather)
    }

    suspend fun deleteForecast(weather: Int ): Int{
        return forecastDao.deleteForecast(weather)
    }



}
