package com.yusuf.app.weather_apps.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.yusuf.app.weather_apps.R
import com.yusuf.app.weather_apps.databinding.ActivityMainBinding
import com.yusuf.app.weather_apps.databinding.BottomSeetFilterBinding
import com.yusuf.app.weather_apps.utils.BasePage
import com.yusuf.app.weather_apps.utils.LinearSpaceDecoration
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.IOException
import java.text.DecimalFormat
import java.util.*


@AndroidEntryPoint
class MainActivity : BasePage(), FilterAdapter.FilterListener{

    private val viewModel: MainViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding

    var lat = -7.2492
    var lon = 112.7508
    var cityName = "surabaya"
    private lateinit var forecastAdapter: ForecastAdapter
    private lateinit var filterAdapter: FilterAdapter

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupObservers()
        getLocation()
        setupRecyclerView()
        setupUi()
    }

    private fun setupUi(){
        binding.city.text = cityName
        binding.city.setOnClickListener {
            bottomSheetCity()
        }

        binding.swipe.setOnRefreshListener {
            setupObservers()
        }
    }


    private fun setupRecyclerView() {
        forecastAdapter = ForecastAdapter()
        binding.rvForecast.apply {
            addItemDecoration(
                LinearSpaceDecoration(
                    space = resources.getDimensionPixelSize(R.dimen._8sdp),
                    customEdge = resources.getDimensionPixelSize(R.dimen._16sdp),
                    includeTop = true,
                    includeBottom = true
                )
            )
            adapter = forecastAdapter
        }

    }

    private fun setupObservers() {
        var weather: Int = 0
        lifecycleScope.launch {
            viewModel.loadweather(cityName).collectLatest {
                try {
                    val data = it
                    binding.data = data
                    val suhu = DecimalFormat("#").format(data.temp)
                    binding.temp.text = suhu.toString() + "°"
                    binding.humidity.text = data.humidity.toString() + "%"
                    weather = data.id
                    lat = data.lat
                    lon = data.lon
                    val iconUrl = "https://openweathermap.org/img/w/${data.icon}.png"
                    Glide
                        .with(this@MainActivity)
                        .load(iconUrl)
                        .centerCrop()
                        .into(binding.img)
                    binding.city.text = data.city
                } catch (e: Exception) {
                    Timber.e("load weather e $e")
                }
            }
        }

        lifecycleScope.launch {
            viewModel.loadForecast(weather, lat, lon).collectLatest {
                try {
                    val data = it

                    if (!data.isNullOrEmpty()) forecastAdapter.setItems(ArrayList(data))
//                    Timber.e("data forecastAdapter $data")
                } catch (e: Exception) {
                    Timber.e("load forecast e $e")
                }
            }
        }
        viewModel.Loading.observe(this, Observer {
            if (it){
                loading()
                binding.swipe.isRefreshing = true
            }else{
                binding.swipe.isRefreshing = false
                dismissloading()
            }




        })

        viewModel.errorMessage.observe(this, Observer {
            if (it != "")
                toast(it)
        })
    }


    private fun getLocation() {
        try {
            val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            if (fusedLocationClient == null) {
                Toast.makeText(this, "Turn ON GPS", Toast.LENGTH_LONG).show()

            } else {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
                fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
                    Timber.e("latitude ${location!!.latitude}")

                    if (location?.latitude != null) {
                        Timber.e("latitude ${location.latitude}")
                        lat = location.latitude
                    }
                    if (location?.longitude != null) {
                        Timber.e("longitude ${location.longitude}")
                        lon = location.longitude
                    }

                }
            }
        } catch (e: IOException) {

        }
    }


    fun bottomSheetCity() {
        val dialog = BottomSheetDialog(this)
        val bindingDialog by lazy { BottomSeetFilterBinding.inflate(layoutInflater) }
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view = bindingDialog.root
        filterAdapter = FilterAdapter(this,dialog)
        lifecycleScope.launch {
            val data = viewModel.filter()
            filterAdapter.setItems(ArrayList(data))
            Timber.e("data category $data")
        }
        bindingDialog.rvCategory.apply {
            layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
            adapter = filterAdapter
        }
        dialog.setContentView(view)
        dialog.show()

    }

    override fun onClickedShortBy(data: String) {
        Timber.e("city name ---  $cityName")
        Timber.e("city name --- 222 $data")
        cityName = data
        binding.city.text = cityName
        setupObservers()
    }


}