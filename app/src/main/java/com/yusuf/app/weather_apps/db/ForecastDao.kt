package com.yusuf.app.weather_apps.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yusuf.app.weather_apps.model.ForecastTable
import kotlinx.coroutines.flow.Flow

@Dao
interface ForecastDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(weather: ForecastTable)

    @Query("SELECT * FROM forecastTable where weather_id =:weather_id ")
    fun getForecast(weather_id: Int): Flow<List<ForecastTable>>

    @Query("SELECT count(*) FROM forecastTable where weather_id =:weather_id   ")
    suspend fun countForecast(weather_id: Int): Int

    @Query("DELETE FROM forecastTable where weather_id =:weather_id   ")
    suspend fun deleteForecast(weather_id: Int): Int

}
