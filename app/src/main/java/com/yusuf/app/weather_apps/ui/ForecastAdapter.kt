package com.yusuf.app.weather_apps.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yusuf.app.weather_apps.databinding.ForecastItemBinding
import com.yusuf.app.weather_apps.model.ForecastTable
import java.text.DecimalFormat

class ForecastAdapter() :
    RecyclerView.Adapter<ForecastViewHolder>() {

    private val items = ArrayList<ForecastTable>()

    fun setItems(items: List<ForecastTable> = emptyList()) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder {
        val binding: ForecastItemBinding =
            ForecastItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ForecastViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) =
        holder.bind(items[position], position)
}

class ForecastViewHolder(
    private val itemBinding: ForecastItemBinding,
) : RecyclerView.ViewHolder(itemBinding.root) {

    private lateinit var data: ForecastTable

    @SuppressLint("SetTextI18n")
    fun bind(item: ForecastTable, position: Int) {
        this.data = item
        val iconUrl = "https://openweathermap.org/img/w/${data.icon}.png"
        Glide
            .with(itemBinding.img)
            .load(iconUrl)
            .centerCrop()
            .into(itemBinding.img)

        itemBinding.txtDay.text = data.day_name
        val suhu = DecimalFormat("#").format(data.temp_average)
        itemBinding.txtTemp.text = "$suhu°"

    }


}