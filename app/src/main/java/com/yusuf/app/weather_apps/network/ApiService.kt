package com.yusuf.app.weather_apps.network


import com.yusuf.app.weather_apps.model.WeatherForecastResponse
import com.yusuf.app.weather_apps.model.WeatherResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("weather")
    suspend fun getWeather(
        @Query("q") city: String,
        @Query("appid") appid: String = "b09e455b8c6b4eb7900bb3349086890b",
    ): Response<WeatherResponse>

  @GET("onecall")
    suspend fun getForecast(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("exclude") exclude: String = "hourly,minutely",
        @Query("appid") appid: String = "b09e455b8c6b4eb7900bb3349086890b",
    ): Response<WeatherForecastResponse>

}