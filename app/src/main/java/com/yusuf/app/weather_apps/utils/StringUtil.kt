package com.yusuf.app.weather_apps.utils

import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class StringUtil @Inject constructor() {

    fun emptyStringToZero(st: String): Int {
        return if (st == "") 0 else st.toInt()
    }

    fun emptyStringToDouble(st: String): Double {
        return if (st == "") 0.0 else st.toDouble()
    }

    private val numberFormatInt by lazy {
        NumberFormat.getCurrencyInstance(Locale("id")).apply {
            maximumFractionDigits = 0
            (this as? DecimalFormat)?.decimalFormatSymbols =
                (this as? DecimalFormat)?.decimalFormatSymbols?.apply { currencySymbol = "" }
        }
    }

    fun formatCurrency(input: Int): String {
        return numberFormatInt.format(input)


    }

    fun convertLongToTime(dt: Long): String {
        val date = Date(dt)
//        val format = SimpleDateFormat("yyyy.MM.dd HH:mm")
        val format = SimpleDateFormat("EEEE, MMMM dd")
        return format.format(date)
    }

    fun getDayName(dt:Long):String{
        val date = Date(dt)
//        val format = SimpleDateFormat("yyyy.MM.dd HH:mm")
        val format = SimpleDateFormat("EEEE")
        return format.format(date)
    }

    fun getCountryName(code: String): String {
        return Locale("", code).displayName
    }

    fun getAverange (tempMin:Double,tempMax:Double):Double{
        return ((tempMin+tempMax)/2) - 273.15 // langsung celcius
    }
}