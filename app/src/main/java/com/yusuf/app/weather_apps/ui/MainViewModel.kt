package com.yusuf.app.weather_apps.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yusuf.app.weather_apps.model.ForecastTable
import com.yusuf.app.weather_apps.model.WeatherTable
import com.yusuf.app.weather_apps.utils.StringUtil
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import timber.log.Timber

class MainViewModel @ViewModelInject constructor(
    private val repository: MainRepository,
    private val stringUtil: StringUtil
) : ViewModel() {
    val Loading: MutableLiveData<Boolean> = MutableLiveData()
    val errorMessage by lazy { MutableLiveData<String>("") }
    var letsLoad: Boolean = true

    fun loadweather(city: String): Flow<WeatherTable> {
        if (letsLoad)
            fetchWeather(city)
        return repository.getWeather(city)
    }

    fun fetchWeather(city: String) = viewModelScope.launch {
        try {
            Loading.value = true
            val response = repository.fetchWeather(city)
            if (response.isSuccessful) {
//                Timber.e("fetchweather ${response.body()}")
                repository.insertWeather(
                    WeatherTable.ResponseToTable(
                        response.body()!!,
                        stringUtil,
                        city
                    )
                )
                loadForecast(
                    response.body()!!.id,
                    response.body()!!.coord.lat,
                    response.body()!!.coord.lon,
                )
                Loading.value = false
            } else {
                Loading.value = false
            }
        } catch (e: Exception) {
            Timber.e("error catch $e")
            errorMessage.postValue(e.message)
            Loading.value = false
        }
    }

    fun loadForecast(weather_id: Int, lat: Double, lon: Double): Flow<List<ForecastTable>> {
        if (letsLoad)
            fetchForecast(weather_id, lat, lon)
        return repository.getForecast(weather_id)
    }

    fun fetchForecast(weather_id: Int, lat: Double, lon: Double) = viewModelScope.launch {
        try {
            Loading.value = true
            val response = repository.fetchForecast(lat, lon)
            if (response.isSuccessful) {
//                Timber.e("fetchweather ${response.body()}")
                if (repository.countForecast(weather_id) > 0) {
                    repository.deleteForecast(weather_id)
                }
                response.body()!!.daily.forEach {
                    repository.insertForecast(
                        ForecastTable.ResponsetoTable(
                            it,
                            weather_id,
                            stringUtil
                        )
                    )
                }
                Loading.value = false
            } else {
                Loading.value = false
            }
        } catch (e: Exception) {
            Timber.e("error catch $e")
            errorMessage.postValue(e.message)
            Loading.value = false
        }
    }

    fun filter(): List<String> {
        var filter = ArrayList<String>()
        filter.add("Gdansk")
        filter.add("Warszawa")
        filter.add("Krakow")
        filter.add("Wroclaw")
        filter.add("Lodz")
        return filter
    }


}
