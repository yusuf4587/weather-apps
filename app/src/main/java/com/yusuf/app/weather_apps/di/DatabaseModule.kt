package com.yusuf.app.weather_apps.di

import android.content.Context
import androidx.room.Room
import com.yusuf.app.weather_apps.db.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun providesDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(
        context,
        AppDatabase::class.java,
        "weather.db"
    ).fallbackToDestructiveMigration().build()


    @Singleton
    @Provides
    fun providesForecastDao(db: AppDatabase) = db.forecastDao()

    @Singleton
    @Provides
    fun providesweatherDao(db: AppDatabase) = db.weatherDao()

}