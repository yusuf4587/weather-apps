package com.yusuf.app.weather_apps.di

import com.yusuf.app.weather_apps.db.ForecastDao
import com.yusuf.app.weather_apps.db.WeatherDao
import com.yusuf.app.weather_apps.network.ApiService
import com.yusuf.app.weather_apps.ui.MainRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
object RepositoryModule {

    @Provides
    fun provideMainRepository(
        apiService: ApiService,
        forecastDao: ForecastDao,
        weatherDao: WeatherDao,
    ): MainRepository {
        return MainRepository(apiService, forecastDao, weatherDao)
    }


}